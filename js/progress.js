
	/************************************************************************************************************
	(C) www.dhtmlgoodies.com, October 2005
	
	This is a script from www.dhtmlgoodies.com. You will find this and a lot of other scripts at our website.	
	
	Terms of use:
	You are free to use this script as long as the copyright message is kept intact. However, you may not
	redistribute, sell or repost it without our permission.
	
	Thank you!
	
	www.dhtmlgoodies.com
	Alf Magne Kalleland
	
	************************************************************************************************************/		
	var progressbar_steps = 50;	// Total number of progress bar steps.
	
	/* Don't change any of these variables */
	var dhtmlgoodies_progressPane = false;
	var dhtmlgoodies_progressBar_bg = false;
	var dhtmlgoodies_progressBar_outer = false;
	var dhtmlgoodies_progressBar_txt = false;
	var progressbarWidth;
	var currentStep = 0;
	function moveProgressBar(steps){
		if(!dhtmlgoodies_progressBar_bg){
			dhtmlgoodies_progressPane = document.getElementById('dhtmlgoodies_progressPane');
			dhtmlgoodies_progressBar_bg = document.getElementById('dhtmlgoodies_progressBar_bg');
			dhtmlgoodies_progressBar_outer = document.getElementById('dhtmlgoodies_progressBar_outer');
			dhtmlgoodies_progressBar_txt = document.getElementById('dhtmlgoodies_progressBar_txt');
			progressbarWidth = dhtmlgoodies_progressBar_bg.clientWidth;
		}
		if(!steps){
			dhtmlgoodies_progressBar_outer.style.width = progressbarWidth + 'px';
			dhtmlgoodies_progressBar_txt.innerHTML = '100%';
			setTimeout('document.getElementById("dhtmlgoodies_progressPane").style.display="none"',50);
		}else{
			currentStep+=steps;
			if(currentStep>progressbar_steps)currentStep = progressbar_steps;
			var width = Math.ceil(progressbarWidth * (currentStep / progressbar_steps));
			dhtmlgoodies_progressBar_outer.style.width = width + 'px';
			var percent = Math.ceil((currentStep / progressbar_steps)*100);
			dhtmlgoodies_progressBar_txt.innerHTML = percent + '%';
			if(currentStep==progressbar_steps){
				setTimeout('document.getElementById("dhtmlgoodies_progressPane").style.display="none"',50);
			}
		}
		
		
		
		
	}
	
	/* The function below is only needed for this demo
	
	To move the progress bar , call the function moveProgressBar() 
	
	example: 
	moveProgressBar(1)
	to move it one step.
	
	*/
	function demoProgressBar()
	{
		if(currentStep<progressbar_steps){
			moveProgressBar(1);
			setTimeout('demoProgressBar()',100);
		}
	}
	
	
	window.onload = demoProgressBar;