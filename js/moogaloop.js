/* author: Florian Weil   ---- www.derhess.de ----- Independent Flash Developer */


///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////// Moogaloop API /////////////////////////////////



function playVimeoVideo()
{
	//alert("Play " + document.getElementById("myFlashID"));
	var vimeoAPI = document.getElementById("myFlashID");
	vimeoAPI.api_play();
	
	addEvents();
}

function pauseVimeoVideo()
{
	//alert("Play " + document.getElementById("myFlashID"));
	var vimeoAPI = document.getElementById("myFlashID");
	vimeoAPI.api_pause();
}

function seekToVimeoVideo(value)
{
	//alert("Play " + document.getElementById("myFlashID"));
	var vimeoAPI = document.getElementById("myFlashID");
	vimeoAPI.api_seekTo(value);
}

function stopVimeoVideo()
{
	//alert("Play " + document.getElementById("myFlashID"));
	var vimeoAPI = document.getElementById("myFlashID");
	vimeoAPI.api_seekTo(0);
	vimeoAPI.api_pause();
}

function unloadVimeoVideo()
{
	//alert("Play " + document.getElementById("myFlashID"));
	var vimeoAPI = document.getElementById("myFlashID");
	vimeoAPI.api_unload();
}


/////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Eventhandling /////////////////////////////////////

function handleCompletEmbedd(e)
{
	myFlashObject = e.ref;
	myFlashIDtext = e.id;
}

function addEvents()
{
	//alert("addEvents");
	var vimeoAPI = document.getElementById("myFlashID");
	
	vimeoAPI.api_addEventListener("onProgress","handleVimeoProgess");
	vimeoAPI.api_addEventListener("onLoading","handleVimeoVideoLoading");
	vimeoAPI.api_addEventListener("onFinish","handleVimeoVideoFinished");
	
}

function handleVimeoProgess(data)
{
	var vimeoAPI = document.getElementById("myFlashID");
	var tim = vimeoAPI.api_getCurrentTime();
	document.getElementById("currentTime").innerHTML = tim;
	document.getElementById("duration").innerHTML = vimeoAPI.api_getDuration();
	//alert('vimeo Progress')
}

function handleVimeoVideoLoading(data)
{
	document.getElementById("loadedBytes").innerHTML = data.bytesLoaded;
	document.getElementById("totalBytes").innerHTML = data.bytesTotal;
	document.getElementById("percentageLoaded").innerHTML = data.percent + '%';
	// Additional Parameter: data.decimal;
}

function handleVimeoVideoFinished()
{
	alert("Video is finished");
}

///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////// Init /////////////////////////////////

var flashvars = {
	'clip_id': '31154352',
	'server': 'vimeo.com',
	'show_title': 0,
	'show_byline': 0,
	'show_portrait': 0,
	'fullscreen': 1,
	'js_api': 1
	}

var parObj = {
	'swliveconnect':true,
	'fullscreen': 1,
	'allowscriptaccess': 'always',
	'allowfullscreen':true
};

var attObj = {}
attObj.id="myFlashID";


swfobject.embedSWF("http://www.vimeo.com/moogaloop.swf", "myContent", "400", "300", "9.0.28", '',flashvars,parObj, attObj );